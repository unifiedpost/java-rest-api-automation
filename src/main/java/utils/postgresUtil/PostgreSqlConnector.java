package utils.postgresUtil;

import utils.TestConfig;

import java.sql.*;
import java.util.ArrayList;

public class PostgreSqlConnector {

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    private static String dbUrl = TestConfig.getDbDriver() + "://" + TestConfig.getDbHostname() + ":" + TestConfig.getDbPort() + "/" + TestConfig.getDbName();
    private static String dbUser = TestConfig.getDbUser();
    private static String dbPassword = TestConfig.getDbPassword();

    public static Connection openConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            try {
                connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
            } catch (SQLException ex) {
                System.out.println("Failed to create the database connection.");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver not found.");
        }
        return connection;
    }

    public static void closeConnection() throws Exception {
        if (connection != null) connection.close();
        if (resultSet != null) resultSet.close();
        if (statement != null) statement.close();
    }

    public static String executeQuery(String query) {
        String result = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                result = resultSet.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Boolean executeQueryForBooleanResult(String query) {
        Boolean result = false;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                result = resultSet.getBoolean(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ArrayList<String> executeQuery(String query, String columnLabel) throws Exception {
        ArrayList<String> results = new ArrayList<>();

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                results.add(resultSet.getString(columnLabel));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }


    public static Connection getConnection() {
        return connection;
    }
}
