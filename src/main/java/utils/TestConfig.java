package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class TestConfig {

    public static String getAlmightyUser() {
        return ConfigProvider.getConfig().getString("test.admin.username");
    }

    public static String getAlmightyPassword() {
        return ConfigProvider.getConfig().getString("test.admin.password");
    }

    public static String getDbDriver() {
        return ConfigProvider.getConfig().getString("test.postgres.driver");
    }

    public static String getDbHostname() {
        return ConfigProvider.getConfig().getString("test.postgres.hostname");
    }

    public static String getDbUser() {
        return ConfigProvider.getConfig().getString("test.postgres.user");
    }

    public static String getDbPassword() {
        return ConfigProvider.getConfig().getString("test.postgres.password");
    }

    public static String getDbPort() {
        return ConfigProvider.getConfig().getString("test.postgres.port");
    }

    public static String getDbName() {
        return ConfigProvider.getConfig().getString("test.postgres.databaseName");
    }

    public static EndpointConnector getEndpointConnector() {
        String baseURI = ConfigProvider.getConfig().getString("test.baseUrl");
        return new EndpointConnector(baseURI);
    }

    public static String getTestrailRunId() {
        return System.getProperty("testRunId");
    }

    public static String getTestrailPlanId() {
        return System.getProperty("testPlanId");
    }

    public static Boolean useTestRail() {
        String envUseTestrail = System.getProperty("useTestrail");
        envUseTestrail = (envUseTestrail != null) ? envUseTestrail : "no";

        return envUseTestrail.equals("yes");
    }

    public static void restoreDb() throws IOException, InterruptedException {
        String path = System.getProperty("user.dir");
        File backupDB = new File(path + "/src/main/resources/MoleQaDB.backup");
        ProcessBuilder builder;
        RunningEnvironment environment = getEnvironment();
        if (environment.equals(RunningEnvironment.JENKINS)) {

            builder = new ProcessBuilder("/usr/pgsql-9.5/bin/pg_restore", "-c", "-h", getDbHostname(), "-p", getDbPort(), "-U", getDbUser(), "-d", getDbName(),
                                         "-v", backupDB.getCanonicalPath());
        } else {
            builder = new ProcessBuilder("pg_restore", "-c", "-h", getDbHostname(), "-p", getDbPort(), "-U", getDbUser(), "-d", getDbName(),
                                         "-v", backupDB.getCanonicalPath());
        }

        builder.redirectErrorStream(true);
        final Process process = builder.start();

        // Watch the process
        watch(process);
        process.waitFor();
    }

    private static void watch(final Process process) {
        new Thread() {
            @Override
            public void run() {
                BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line = null;
                try {
                    while ((line = input.readLine()) != null) {
                        System.out.println(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private static RunningEnvironment getEnvironment() throws UnknownHostException {
        InetAddress ip = InetAddress.getLocalHost();
        if (ip.getHostName().contains("jenkinsmaster")) {
            return RunningEnvironment.JENKINS;
        } else {
            return RunningEnvironment.LOCAL;
        }
    }

    private enum RunningEnvironment {
        LOCAL,
        JENKINS;
    }

    public enum TestRailStatus {
        PASSED("1"),
        BLOCKED("2"),
        UNTESTED("3"),
        RETEST("4"),
        FAILED("5"),
        SKIPPED("6");

        private final String statusId;

        TestRailStatus(String statusId) {
            this.statusId = statusId;
        }

        public String getStatusId() {
            return statusId;
        }
    }
}
