package utils;

import api.GetRequest;
import api.Request;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import queries.PostgresQueries;
import utils.postgresUtil.PostgreSqlConnector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Helpers {

    private Helpers() {
        throw new IllegalMonitorStateException("Utility class");
    }

    public static String setBasicAuthForUser(String username, String password) {
        String authString = username + ":" + password;
        return Base64.getEncoder().encodeToString(authString.getBytes());
    }

    public static String generateRandom() {
        return String.valueOf(new Random(System.currentTimeMillis()).nextInt(99999999));
    }

    public static String generateRandomAlphaNumericString() {
        return RandomStringUtils.randomAlphanumeric(8).toUpperCase();
    }

    public static void retryGetRequestUntilResponseOk(GetRequest getRequest) throws IOException, InterruptedException {
        long milliseconds = 2000;
        do {
            Thread.sleep(milliseconds);
            getRequest.executeRequest();
            milliseconds = milliseconds + 2000;
        } while (getRequest.getResponseCode() == HttpStatus.SC_NOT_FOUND && milliseconds <= 300000);
    }

    public static void setBasicAuth(String username, String password) {
        Request.basicAuth = setBasicAuthForUser(username, password);
    }

    public static Document parseXmlFile(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        return dBuilder.parse(file);
    }

}
