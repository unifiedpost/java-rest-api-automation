package utils;

public class EndpointConnector {
    private String baseUrl;

    public EndpointConnector(String baseUrl) {

        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
