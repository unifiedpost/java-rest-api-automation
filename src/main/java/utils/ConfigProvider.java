package utils;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;

public class ConfigProvider {

    private ConfigProvider() {
        throw new IllegalStateException("Config class");
    }

    private static CompositeConfiguration config;

    static {
        config = new CompositeConfiguration();
        config.addConfiguration(new SystemConfiguration());
        try {
            config.addConfiguration(new PropertiesConfiguration("configuration.properties"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static CompositeConfiguration getConfig() {
        return config;
    }

}
