package api;

import com.google.gson.JsonObject;

public class ErrorValidation {

    public String getError(JsonObject jsonObject) {
        return jsonObject.get("error").getAsString();
    }

    public String getMessageError(JsonObject jsonObject) {
        return jsonObject.get("message").getAsString();
    }

    public enum Errors {
        BAD_REQUEST_EXCEPTION("", "BadRequestException");

        private String errorMessage;
        private String errorCode;

        Errors(String errorCode, String errorAsString) {
            this.errorCode = errorCode;
            this.errorMessage = errorAsString;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public String getErrorCode() {
            return errorCode;
        }
    }

}
