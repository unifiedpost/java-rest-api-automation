package api;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

public class DeleteRequest extends Request {
    private final String endpoint;

    public DeleteRequest(String endpoint) {
        this.endpoint = endpoint;
    }

    public void executeRequest() throws IOException {
        response = executeDeleteRequest();
    }

    private HttpResponse executeDeleteRequest() throws IOException {
        CloseableHttpClient httpClient = establishHttpsConnection();
        HttpDelete request = new HttpDelete(endpoint);
        setHeaders(request);
        return httpClient.execute(request);
    }
}
