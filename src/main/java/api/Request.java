package api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class Request {

    public static String basicAuth;
    HttpResponse response;
    private static final String CERTIFICATE_PATH = "/src/main/resources/certificate.jks";
    private static final String KEYSTORE_PASSWORD = "password";

    static CloseableHttpClient establishHttpsConnection() throws IOException {
        String path = System.getProperty("user.dir");
        File keyStore = new File(path + CERTIFICATE_PATH);
        SSLContext sslcontext = null;
        try {
            sslcontext = SSLContexts.custom()
                    .loadTrustMaterial(keyStore, KEYSTORE_PASSWORD.toCharArray(),
                                       new TrustSelfSignedStrategy())
                    .build();
        } catch (NoSuchAlgorithmException | CertificateException | KeyStoreException | KeyManagementException | IOException e) {
            e.printStackTrace();
        }

        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                sslcontext,
                new String[]{"TLSv1"},
                null,
                hostnameVerifier);

        return HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .build();
    }

    void setHeaders(HttpRequest request) {
        request.addHeader("Content-Type", "application/json");
        request.addHeader("Authorization", "Basic " + basicAuth);
    }

    public JsonArray getJsonArrayFromResponse() throws IOException {
        String responseBody = EntityUtils.toString(response.getEntity());
        return (new JsonParser()).parse(responseBody).getAsJsonArray();
    }

    public JsonObject getJsonObjectFromResponse() throws IOException {
        String responseBody = EntityUtils.toString(response.getEntity());
        return (new JsonParser()).parse(responseBody).getAsJsonObject();
    }

    public String getStringFromResponse() throws IOException {
        return EntityUtils.toString(response.getEntity());
    }

    public Path downloadFileFromResponse() throws IOException {
        Header[] headers = response.getHeaders("Content-disposition");

        String fileName = headers[0].getValue().replaceFirst("(?i)^.*filename=\"([^\"]+)\".*$", "$1");
        Path filePath = Paths.get(System.getProperty("user.dir") + "/src/test/resources/DownloadedFiles/" + fileName);

        try (InputStream in = response.getEntity().getContent()) {
            Files.copy(in, filePath);
        }
        return filePath;
    }

    public int getResponseCode() {
        return response.getStatusLine().getStatusCode();
    }

    public HttpResponse getResponse() {
        return response;
    }
}
