package api;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.File;
import java.io.IOException;

import static api.PostRequest.RequestType.*;

public class PostRequest extends Request {

    private final String endpoint;
    private final String requestBody;
    private final String multiPartParamName;
    private final File fileToUpload;

    public PostRequest(PostRequestBuilder builder) {
        this.endpoint = builder.endpoint;
        this.requestBody = builder.requestBody;
        this.multiPartParamName = builder.multiPartParamName;
        this.fileToUpload = builder.fileToUpload;
    }

    public void executeRequest() throws IOException {
        response = executePostRequest();
    }

    private HttpResponse executePostRequest() throws IOException {
        CloseableHttpClient httpClient = establishHttpsConnection();
        HttpPost request = new HttpPost(endpoint);
        RequestType requestType = setRequestType();
        switch (requestType) {
            case ONLY_HEADERS: {
                return getResponseFromRequestWithRequestBody(httpClient, request, "");
            }
            case WITH_REQUEST_BODY: {
                return getResponseFromRequestWithRequestBody(httpClient, request, requestBody);
            }
            case WITH_FILE_UPLOAD: {
                return getResponseFromRequestWithFileUpload(httpClient, request);
            }
            case WITH_FILE_UPLOAD_AND_REQUEST_BODY: {
                return getResponseFromRequestWithFileUploadAndRequestBody(httpClient, request);
            }
            default:
                return null;
        }

    }

    private HttpResponse getResponseFromRequestWithFileUploadAndRequestBody(CloseableHttpClient httpClient, HttpPost request) throws IOException {
        request.addHeader("Authorization", "Basic " + basicAuth);

        StringBody json = new StringBody(requestBody, ContentType.APPLICATION_JSON);
        FileBody uploadFilePart = new FileBody(fileToUpload);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        builder.addPart("mandateDto", json);
        builder.addPart(multiPartParamName, uploadFilePart);

        HttpEntity entity = builder.build();
        request.setEntity(entity);

        return httpClient.execute(request);
    }

    private HttpResponse getResponseFromRequestWithFileUpload(CloseableHttpClient httpClient, HttpPost request) throws IOException {
        request.addHeader("Authorization", "Basic " + basicAuth);

        FileBody uploadFilePart = new FileBody(fileToUpload);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addPart(multiPartParamName, uploadFilePart);

        HttpEntity entity = builder.build();
        request.setEntity(entity);

        return httpClient.execute(request);
    }

    private HttpResponse getResponseFromRequestWithRequestBody(CloseableHttpClient httpClient, HttpPost request, String string) throws IOException {
        setHeaders(request);
        StringEntity jsonBody = new StringEntity(string);
        request.setEntity(jsonBody);
        return httpClient.execute(request);
    }

    private RequestType setRequestType() {
        if (requestBody != null && fileToUpload == null) return WITH_REQUEST_BODY;
        else if (requestBody == null && fileToUpload != null) return WITH_FILE_UPLOAD;
        else if (requestBody != null && fileToUpload != null) return WITH_FILE_UPLOAD_AND_REQUEST_BODY;
        else return ONLY_HEADERS;
    }

    public enum RequestType {
        ONLY_HEADERS,
        WITH_REQUEST_BODY,
        WITH_FILE_UPLOAD,
        WITH_FILE_UPLOAD_AND_REQUEST_BODY;
    }

    public static class PostRequestBuilder {
        private String endpoint;
        private String requestBody;
        private String multiPartParamName;
        private File fileToUpload;

        public PostRequestBuilder endpoint(String endpoint) {
            this.endpoint = endpoint;
            return this;
        }

        public PostRequestBuilder requestBody(String requestBody) {
            this.requestBody = requestBody;
            return this;
        }

        public PostRequestBuilder multiPartParamName(String multiPartParamName) {
            this.multiPartParamName = multiPartParamName;
            return this;
        }

        public PostRequestBuilder fileToUpload(File fileToUpload) {
            this.fileToUpload = fileToUpload;
            return this;
        }

        public PostRequest build() {
            return new PostRequest(this);
        }
    }

}

