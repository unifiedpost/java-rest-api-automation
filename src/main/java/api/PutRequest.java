package api;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

public class PutRequest extends Request {
    private final String endpoint;
    private final String requestBody;

    public PutRequest(String endpoint, String requestBody) throws IOException {
        this.endpoint = endpoint;
        this.requestBody = requestBody;
    }

    public void executeRequest() throws IOException {
        response = executePutRequest();
    }


    private HttpResponse executePutRequest() throws IOException {
        CloseableHttpClient httpClient = establishHttpsConnection();
        HttpPut putRequest = new HttpPut(endpoint);
        setHeaders(putRequest);
        StringEntity jsonBody = new StringEntity(requestBody);
        putRequest.setEntity(jsonBody);
        return httpClient.execute(putRequest);
    }
}
