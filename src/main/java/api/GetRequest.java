package api;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

public class GetRequest extends Request {

    private final String endpoint;

    public GetRequest(String endpoint) {
        this.endpoint = endpoint;
    }

    public void executeRequest() throws IOException {
        response = executeGetRequest();
    }

    private HttpResponse executeGetRequest() throws IOException {
        CloseableHttpClient httpClient = establishHttpsConnection();
        HttpGet request = new HttpGet(endpoint);
        setHeaders(request);
        return httpClient.execute(request);
    }
}
