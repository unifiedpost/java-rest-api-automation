package com.unifiedpost.mole.automation.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TestrailCaseId {
    String value() default "";
}
