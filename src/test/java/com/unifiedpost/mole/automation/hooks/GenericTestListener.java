package com.unifiedpost.mole.automation.hooks;

import com.unifiedpost.mole.automation.annotations.TestrailCaseId;
import gurock.testrail.APIException;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import utils.TestConfig;
import utils.TestrailClient;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

public class GenericTestListener extends TestListenerAdapter {
    TestrailClient tc;

    @Override
    public void onTestSkipped(ITestResult tr) {
        String bsResultPage = "";

        if (TestConfig.useTestRail()) {
            String comment = buildConfTraceMessage(tr);
            sendResultToTestrail(tr, comment, TestConfig.TestRailStatus.SKIPPED.getStatusId());
        }
    }

    @Override
    public void onTestFailure(ITestResult tr) {

        if (TestConfig.useTestRail()) {
            String comment = buildErrorTraceMessage(tr);
            sendResultToTestrail(tr, comment, TestConfig.TestRailStatus.FAILED.getStatusId());
        }
    }

    @Override
    public void onTestSuccess(ITestResult tr) {

        if (TestConfig.useTestRail()) {
            sendResultToTestrail(tr, "", TestConfig.TestRailStatus.PASSED.getStatusId());
        }
    }


    private String buildErrorTraceMessage(ITestResult tr) {
        return "Error trace" + System.lineSeparator()
                + "=============" + System.lineSeparator() + getErrorTrace(tr);

    }

    private String buildConfTraceMessage(ITestResult tr) {
        return "Configuration error trace" + System.lineSeparator()
                + "=============" + System.lineSeparator() + getConfErrorTrace();
    }

    private void sendResultToTestrail(ITestResult tr, String comment, String status) {
        if (TestConfig.useTestRail()) {
            String planId = TestConfig.getTestrailPlanId();

            if (planId != null) {
                sendResultToTestrailPlan(getTestCaseId(tr), comment, planId, status);
            } else {
                String runId = TestConfig.getTestrailRunId();
                if (runId != null) {
                    sendResultToTestrailRun(getTestCaseId(tr), comment, runId, status);
                } else {
                    System.out.println("Neither test plan id nor test run id have been provided");
                }
            }

        }
    }

    private String getErrorTrace(ITestResult tr) {
        String errorTrace = tr.getThrowable().getMessage() + System.lineSeparator() + System.lineSeparator();

        StackTraceElement[] stackTrace = tr.getThrowable().getStackTrace();
        for (StackTraceElement ste : stackTrace) {
            errorTrace += ste.toString() + System.lineSeparator();
        }
        return errorTrace;
    }


    private String getConfErrorTrace() {
        String confErrorTrace = "";

        List<ITestResult> confResultList = this.getConfigurationFailures();

        for (ITestResult confResult : confResultList) {
            confErrorTrace += confResult.getThrowable().getMessage() + System.lineSeparator() + System.lineSeparator();
            StackTraceElement[] stackTrace = confResult.getThrowable().getStackTrace();
            for (StackTraceElement ste : stackTrace) {
                confErrorTrace += ste.toString() + System.lineSeparator();
            }
            confErrorTrace += System.lineSeparator() + System.lineSeparator();
        }

        return confErrorTrace;
    }

    private String getTestCaseId(ITestResult tr) {
        Method method = tr.getMethod().getConstructorOrMethod().getMethod();
        TestrailCaseId tcId = method.getAnnotation(TestrailCaseId.class);
        return tcId.value().substring(1);
    }


    private void sendResultToTestrailRun(String testCaseId, String comment, String runId, String statusId) {
        tc = new TestrailClient();
        try {
            tc.sendTestResultInRun(runId, testCaseId, statusId, comment, null);
        } catch (IOException | APIException e) {
            e.printStackTrace();
        }
    }

    private void sendResultToTestrailPlan(String testCaseId, String comment, String planId, String statusId) {
        tc = new TestrailClient();
        try {
            tc.sendTestResultInPlan(planId, testCaseId, statusId, comment, null);
        } catch (IOException | APIException e) {
            e.printStackTrace();
        }
    }

}