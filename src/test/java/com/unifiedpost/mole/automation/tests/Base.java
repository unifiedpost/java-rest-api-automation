package com.unifiedpost.mole.automation.tests;

import api.Request;
import com.unifiedpost.mole.automation.hooks.GenericTestListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import utils.EndpointConnector;
import utils.Helpers;
import utils.TestConfig;

import java.io.IOException;

@Listeners({GenericTestListener.class})
public class Base {

    static final String RESOURCE_FOLDER_PATH = System.getProperty("user.dir") + "/src/test/resources";

    EndpointConnector endpointConnector;

    private void setBasicAuth() {
        String username = TestConfig.getAlmightyUser();
        String password = TestConfig.getAlmightyPassword();
        Request.basicAuth = Helpers.setBasicAuthForUser(username, password);
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        endpointConnector = TestConfig.getEndpointConnector();
        setBasicAuth();
    }

    @BeforeSuite
    public void restoreDb() throws IOException, InterruptedException {
        TestConfig.restoreDb();
    }
}

