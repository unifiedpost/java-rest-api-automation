package com.unifiedpost.mole.automation.tests;

import api.GetRequest;
import com.unifiedpost.mole.automation.annotations.TestrailCaseId;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class AuthorizedUsersForRequestsTests extends Base {


    @Test
    @TestrailCaseId("")
    public void verifyAdminIsAuthorizedToExecuteGetRequest() throws IOException {
        GetRequest getRequest = new GetRequest(endpointConnector.getBaseUrl());

        getRequest.executeRequest();

        Assert.assertEquals(getRequest.getResponseCode(), HttpStatus.SC_OK);

    }

}
